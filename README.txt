-- DESCRIPTION --
Apache Solr Advanced Search module provides users with advanced search options 
using apache solr.User can perform a more specific search by using the options 
such as search containing all of these words ,Exact phrase,Any of these words or
None of these words.User can also choose how many results should be shown per 
page during every search.This module also provides option to allow user to 
search in specific content type.

-- REQUIREMENTS --

Apache Solr Search Integration
  http://drupal.org/project/apachesolr

-- INSTALLATION AND USAGE--

* Install as usual

Advanced search page for apache solr will be available at 
url www.example.com/advancedsearch

For configuration options for advanced search

1. Go to Site Configuration � Apache Solr � Advanced search Settings

2. Configure options to show apache solr advanced search link below 
   the search forms.

3. Configure whether to allow user to search in specific content type.

4. Click Save to save the settings.
